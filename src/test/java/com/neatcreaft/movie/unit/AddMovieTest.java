package com.neatcreaft.movie.unit;

import com.neatcreaft.movie.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddMovieTest {
	@Test
	@DisplayName("Should insert new movie when it's ok")
	void addMovieWhenOk() throws Exception {
		final List<MovieEntity> movies = new ArrayList<>();
		final MovieController movieController = init(movies);
		final Movie resource = new Movie("edede");


		movieController.insert(resource);

		assertEquals(1, movies.size());
	}

	@Test
	@DisplayName("Doit enregistrer le titre du film quand il est renseigné")
	public void saveTitle() throws Exception {
		final String expected = "keeknfeknfe";
		final List<MovieEntity> movies = new ArrayList<>();
		final MovieController movieController = init(movies);
		final Movie resource = new Movie(expected);

		movieController.insert(resource);

		assertEquals(expected, movies.get(0).getTitle());
	}

	@Test
	@DisplayName("Doit emettre une erreur quand le titre n'est pas renseigné")
	public void emitErrorWhenTitleNotFilled() {
		final List<MovieEntity> movies = new ArrayList<>();
		final MovieController movieController = init(movies);
		final Movie resource = new Movie(null);

		Assertions.assertThrows(Exception.class, () -> movieController.insert(resource));
	}

	@Test
	@DisplayName("Doit enregistrer le date de sortie du film quand il est renseigné")
	public void saveReleaseDate() throws Exception {
		final Date expected = new Date();
		final List<MovieEntity> movies = new ArrayList<>();
		final MovieController movieController = init(movies);
		final Movie resource = new Movie("efefe");

		resource.setReleaseAt(expected);

		movieController.insert(resource);

		assertEquals(expected, movies.get(0).getReleaseAt());
	}

	@ParameterizedTest(name = "{0}")
	@EnumSource(MovieType.class)
	@DisplayName("Doit enregister le genre du film quand il est renseigné")
	public void saveFantaisyKindWhenItsOk(MovieType movieType) throws Exception {
		final Set<MovieType> expected = EnumSet.of(movieType);
		final List<MovieEntity> movies = new ArrayList<>();
		final MovieController movieController = init(movies);
		final Movie resource = new Movie("efefe");

		resource.setMovieType(expected);

		movieController.insert(resource);

		assertEquals(expected.stream().findFirst().get().getFlag(), movies.get(0).getMovieType());
	}

	@Test
	@DisplayName("Doit enregister le genre comédie et action du film quand il est renseigné")
	public void saveComedyAndActionKindWhenItsOk() throws Exception {
		final Set<MovieType> expected = EnumSet.of(MovieType.COMEDY, MovieType.ACTION);
		final List<MovieEntity> movies = new ArrayList<>();
		final MovieController movieController = init(movies);
		final Movie resource = new Movie("efefe");

		resource.setMovieType(expected);

		movieController.insert(resource);

		assertEquals(MovieType.COMEDY.getFlag() | MovieType.ACTION.getFlag(), movies.get(0).getMovieType());
	}

	final MovieController init(List<MovieEntity> movies) {
		final MovieRepository movieRepository = new MovieRepository(new MovieDao() {
			@Override
			public void insert(MovieEntity movie) {
				movies.add(movie);
			}
		});
		final MovieService movieService = new MovieService(movieRepository);
		return new MovieController(movieService);
	}

}
