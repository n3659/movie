package com.neatcreaft.movie;

public enum MovieType {
	HEROIC_FANTASY(1),
	SIENCE_FICTION(1<<1),
	COMEDY(1<<2),
	ACTION(1<<3);


	private final long flag;

	MovieType(long flag) {

		this.flag = flag;
	}

	public long getFlag() {
		return this.flag;
	}
}
