package com.neatcreaft.movie;

import java.util.Date;
import java.util.Set;

public class Movie {
	private final String title;
	private Date releaseAt;
	private Set<MovieType> movieType;

	public Movie(String title) {
		this.title = title;
	}

	public String getTitle() {
		return this.title;
	}

	public void setReleaseAt(Date releaseAt) {
		this.releaseAt = releaseAt;
	}

	public Date getReleaseAt() {
		return this.releaseAt;
	}

	public void setMovieType(Set<MovieType> movieType) {
		this.movieType = movieType;
	}

	public Set<MovieType> getMovieType() {
		return this.movieType;
	}
}
