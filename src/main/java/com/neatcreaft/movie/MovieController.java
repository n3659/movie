package com.neatcreaft.movie;

public class MovieController {
	private final MovieService movieService;

	public MovieController(final MovieService movieService) {
		this.movieService = movieService;
	}

	public void insert(Movie resource) throws Exception {
		if (resource.getTitle() == null) {
			throw new Exception();
		}

		this.movieService.insert(resource);
	}
}
