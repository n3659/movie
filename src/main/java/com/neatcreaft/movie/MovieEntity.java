package com.neatcreaft.movie;

import java.util.Date;
import java.util.Set;

public class MovieEntity {
	private final String title;
	private Date releaseAt;
	private long movieType;

	public MovieEntity(String title) {
		this.title = title;
	}

	public String getTitle() {
		return this.title;
	}

	public void setReleaseAt(Date releaseAt) {
		this.releaseAt = releaseAt;
	}

	public Date getReleaseAt() {
		return this.releaseAt;
	}


	public void addMovieType(MovieType movieType) {
		this.movieType = this.movieType | movieType.getFlag();
	}

	public long getMovieType() {
		return this.movieType;
	}
}
