package com.neatcreaft.movie;

public class MovieRepository {
	private final MovieDao movieDao;

	public MovieRepository(MovieDao movieDao) {
		this.movieDao = movieDao;
	}

	void insert(Movie movie) {
		final MovieEntity entity = new MovieEntity(movie.getTitle());

		entity.setReleaseAt(movie.getReleaseAt());

		if (movie.getMovieType() != null) {
			movie.getMovieType().forEach(entity::addMovieType);
		}

		this.movieDao.insert(entity);
	}
}
