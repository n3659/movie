package com.neatcreaft.movie;

public class MovieService {
	private final MovieRepository movieRepository;

	public MovieService(final MovieRepository movieRepository) {
		this.movieRepository = movieRepository;
	}

	public void insert(Movie movie) {
		this.movieRepository.insert(movie);
	}
}
